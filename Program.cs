﻿using System;
using Ostis.Sctp;
using Ostis.Sctp.Arguments;

namespace RecordConcatenation
{
    class Program
    {
        static void Main(string[] args)
        {

            Identifier product = new Identifier("product");
            Identifier product_units_of_measure = new Identifier("product_units_of_measure");
            Identifier mergedProduct = new Identifier("mergedProduct");

            var concatenator = new Concatenator("192.168.100.3", SctpProtocol.DefaultPortNumber);
            concatenator.Join(mergedProduct, product, product_units_of_measure, "Новая запись");

            Console.WriteLine("Done!");
            Console.ReadKey();
        }
    }
}
