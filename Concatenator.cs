﻿using System;
using System.Linq;
using Ostis.Sctp;
using Ostis.Sctp.Arguments;
using Ostis.Sctp.Commands;
using Ostis.Sctp.Responses;
using Ostis.Sctp.CompositeCommands;
using System.Collections.Generic;

namespace RecordConcatenation
{
    class Concatenator
    {
        private readonly Identifier rrel_record_field = "rrel_record_field";
        private readonly Identifier rrel_record_first_field = "rrel_record_first_field";
        private readonly Identifier nrel_next_item = "nrel_next_item";
        private readonly Identifier nrel_system_identifier = "nrel_system_identifier";
        private readonly Identifier rrel_record_primaryKey = "rrel_record_primaryKey";


        SctpClient client;
        public Concatenator(string address, int port)
        {
            client = new SctpClient(address, port);
            client.Connect();
        }

        private ScAddress FindFirstField(ScAddress record)
        {
            ScAddress predicate = Commands.FindAddressByIdentifier(client, rrel_record_first_field);
            var template = new ConstructionTemplate(record, ElementType.PositiveConstantPermanentAccessArc_c,
                ElementType.ConstantNode_c, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
            var cmdIterateElements = new IterateElementsCommand(template);
            var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

            if (rspIterateElements.Constructions.Count == 1)
            {
                return rspIterateElements.Constructions[0][2];
            }

            return ScAddress.Invalid;
        }

        private Identifier JoinIdentifiers(Identifier first, Identifier second)
        {
            return new Identifier(first.ToString() + "." + second.ToString());
        }

        private void Push(ScAddress record, ScAddress targetField)
        {
            ScAddress field = FindFirstField(record);
            ScAddress predicate = Commands.FindAddressByIdentifier(client, nrel_next_item);

            while (true)
            {
                var template = new ConstructionTemplate(field, ElementType.ConstantCommonArc_c,
                     ElementType.ConstantNode_c, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
                var cmdIterateElements = new IterateElementsCommand(template);
                var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

                if (rspIterateElements.Constructions.Count() > 0)
                {
                    field = rspIterateElements.Constructions[0][2];
                }
                else
                {
                    Commands.CreateArc(client, ElementType.ConstantCommonArc_c, field, targetField, nrel_next_item);
                    break;
                }
            }
        }

        private void CreateRecordId(ScAddress record, Identifier identifier)
        {
            ScAddress node = Commands.CreateNode( client, ElementType.ConstantNode_c,
                JoinIdentifiers(identifier, new Identifier("id")));
            string nodeName = "ID" + "(" +
                (Commands.FindStringLinkContent(client, Commands.FindNodeMainIdtf(client, record))) + ")";
            Commands.CreateNodeMainIdtf(client, node,  nodeName);
            ScAddress predicate = Commands.FindAddressByIdentifier(client, rrel_record_field);

            Commands.CreateArc(client, ElementType.PositiveConstantPermanentAccessArc_c, record, node, rrel_record_field);
            Commands.CreateArc(client, ElementType.PositiveConstantPermanentAccessArc_c, record, node, rrel_record_first_field);
            Commands.CreateArc(client, ElementType.PositiveConstantPermanentAccessArc_c, record, node, rrel_record_primaryKey);

        }

        private Tuple<Identifier, Identifier> ParseComplexIdentifier(Identifier target)
        {
            string[] names = target.ToString().Split('.');

            return Tuple.Create(new Identifier(names[0]), new Identifier(names[1]));
        }

        private Tuple<string, string> ParseComplexName(string target)
        {
            string[] names = target.ToString().Split('(');

            return Tuple.Create(names[0], names[1]);
        }


        private void CreateMainIdtf(ScAddress table, ScAddress parentNode, ScAddress target)
        {
            ScAddress tableAddress = Commands.FindNodeMainIdtf(client, table);
            ScAddress parentAddress = Commands.FindNodeMainIdtf(client, parentNode);

            if (tableAddress == ScAddress.Invalid || parentAddress == ScAddress.Invalid) return;

            //string tableName = LinkContent.ToString(Commands.FindLinkContent(client, tableAddress).GetBytes());
            //string parentName = LinkContent.ToString(Commands.FindLinkContent(client, parentAddress).GetBytes());

            string tableName = Commands.FindStringLinkContent(client, tableAddress);
            string parentName = Commands.FindStringLinkContent(client, parentAddress);


            Tuple<string, string> names = ParseComplexName(parentName);

            string targetName = names.Item1 + "(" + tableName + ")";
            Commands.CreateNodeMainIdtf(client, target, targetName);
        }

        private ScAddress CreateRecordCopy(ScAddress record, Identifier identifier, string newRecordMainIdtf)
        {
            ScAddress copy = Commands.CreateNode(client, ElementType.ConstantNode_c, identifier);
            Commands.CreateNodeMainIdtf(client, copy, newRecordMainIdtf);
            ScAddress predicate = Commands.FindAddressByIdentifier(client, nrel_next_item);

            CreateRecordId(copy, identifier);
            

            ScAddress node = FindFirstField(record);

            while(true)
            {
                Identifier nodeId = Commands.FindNodeSysIdentifierByAddress(client, node);
                if (nodeId == Identifier.Invalid)
                {
                    return ScAddress.Invalid;
                }

                Tuple<Identifier, Identifier> ids = ParseComplexIdentifier(nodeId);

                ScAddress copyNode = Commands.CreateNode(client, ElementType.ConstantNode_c,
                    JoinIdentifiers(identifier, ids.Item2));

                CreateMainIdtf(copy, node, copyNode);

                Push(copy, copyNode);
                Commands.CreateArc(client, ElementType.PositiveConstantPermanentAccessArc_c, copy, copyNode, rrel_record_field);

                var template = new ConstructionTemplate(node, ElementType.ConstantCommonArc_c,
                ElementType.ConstantNode_c, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
                var cmdIterateElements = new IterateElementsCommand(template);
                var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

                if (rspIterateElements.Constructions.Count() != 1) break;
                node = rspIterateElements.Constructions[0][2];
            }
            return copy;
        }


        public void Join(Identifier identifier,  Identifier firstRecordId, Identifier secondRecordId, string recordName="")
        {
            ScAddress firstRecord = Commands.FindAddressByIdentifier(client, firstRecordId);
            ScAddress secondRecord = Commands.FindAddressByIdentifier(client, secondRecordId);

            ScAddress resultRecord = CreateRecordCopy(firstRecord, identifier, recordName);

            ScAddress node = FindFirstField(secondRecord);
            ScAddress predicate = Commands.FindAddressByIdentifier(client, nrel_next_item);

            while (true)
            {
                Identifier nodeId = Commands.FindNodeSysIdentifierByAddress(client, node);
                Tuple<Identifier, Identifier> ids = ParseComplexIdentifier(nodeId);

                if (Commands.FindAddressByIdentifier(client, JoinIdentifiers(identifier, ids.Item2)) == ScAddress.Invalid)
                {
                    ScAddress copyNode = Commands.CreateNode(client, ElementType.ConstantNode_c,
                        JoinIdentifiers(identifier, ids.Item2));
                    CreateMainIdtf(resultRecord, node, copyNode);
                    Push(resultRecord, copyNode);
                    Commands.CreateArc(client, ElementType.PositiveConstantPermanentAccessArc_c,
                        resultRecord, copyNode, rrel_record_field);
                }

                var template = new ConstructionTemplate(node, ElementType.ConstantCommonArc_c,
                ElementType.ConstantNode_c, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
                var cmdIterateElements = new IterateElementsCommand(template);
                var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

                if (rspIterateElements.Constructions.Count() != 1) break;
                node = rspIterateElements.Constructions[0][2];
            }
        }
    }
}
